"""M3C 2018 Homework 3
Yage Hao CID:01184355
Contains five functions:
    plot_S: plots S matrix -- use if you like
    simulate2: Simulate tribal competition over m trials. Return: all s matrices at final time
        and fc at nt+1 times averaged across the m trials.
    performance: To be completed -- analyze and assess performance of python, fortran, and fortran+openmp simulation codes
    analyze: To be completed -- analyze influence of model parameter, g
    visualize: To be completed -- generate animation illustrating "interesting" tribal dynamics
"""
import numpy as np
import matplotlib.pyplot as plt
from m1 import tribes as tr #assumes that hw3_dev.f90 has been compiled with: f2py --f90flags='-fopenmp' -c hw3_dev.f90 -m m1 -lgomp
#May also use scipy and time modules as needed
import time
import matplotlib.animation as animation

def plot_S(S):
    """Simple function to create plot from input S matrix
    """
    ind_s0 = np.where(S==0) #C locations
    ind_s1 = np.where(S==1) #M locations
    plt.plot(ind_s0[1],ind_s0[0],'rs')
    plt.plot(ind_s1[1],ind_s1[0],'bs')
    plt.show()
    return None
#------------------

def simulate2(N,Nt,b,e,g,m):
    """Simulate m trials of C vs. M competition on N x N grid over
    Nt generations. b, e, and g are model parameters
    to be used in fitness calculations.
    Output: S: Status of each gridpoint at end of simulation, 0=M, 1=C
            fc_ave: fraction of villages which are C at all Nt+1 times
                    averaged over the m trials
    """
    #Set initial condition
    S  = np.ones((N,N,m),dtype=int) #Status of each gridpoint: 0=M, 1=C
    j = int((N-1)/2)
    S[j,j,:] = 0
    N2inv = 1./(N*N)

    fc_ave = np.zeros(Nt+1) #Fraction of points which are C
    fc_ave[0] = S.sum()

    #Initialize matrices
    NB = np.zeros((N,N,m),dtype=int) #Number of neighbors for each point
    NC = np.zeros((N,N,m),dtype=int) #Number of neighbors who are Cs
    S2 = np.zeros((N+2,N+2,m),dtype=int) #S + border of zeros
    F = np.zeros((N,N,m)) #Fitness matrix
    F2 = np.zeros((N+2,N+2,m)) #Fitness matrix + border of zeros
    A = np.ones((N,N,m)) #Fitness parameters, each of N^2 elements is 1 or b
    P = np.zeros((N,N,m)) #Probability matrix
    Pden = np.zeros((N,N,m))
    #---------------------

    #Calculate number of neighbors for each point
    NB[:,:,:] = 8
    NB[0,1:-1,:],NB[-1,1:-1,:],NB[1:-1,0,:],NB[1:-1,-1,:] = 5,5,5,5
    NB[0,0,:],NB[-1,-1,:],NB[0,-1,:],NB[-1,0,:] = 3,3,3,3
    NBinv = 1.0/NB
    #-------------

    #----Time marching-----
    for t in range(Nt):
        R = np.random.rand(N,N,m) #Random numbers used to update S every time step

        #Set up coefficients for fitness calculation
        A = np.ones((N,N,m))
        ind0 = np.where(S==0)
        A[ind0] = b

        #Add boundary of zeros to S
        S2[1:-1,1:-1,:] = S

        #Count number of C neighbors for each point
        NC = S2[:-2,:-2,:]+S2[:-2,1:-1,:]+S2[:-2,2:,:]+S2[1:-1,:-2,:] + S2[1:-1,2:,:] + S2[2:,:-2,:] + S2[2:,1:-1,:] + S2[2:,2:,:]

        #Calculate fitness matrix, F----
        F = NC*A
        F[ind0] = F[ind0] + (NB[ind0]-NC[ind0])*e
        F = F*NBinv
        #-----------

        #Calculate probability matrix, P-----
        F2[1:-1,1:-1,:]=F
        F2S2 = F2*S2
        #Total fitness of cooperators in community
        P = F2S2[:-2,:-2,:]+F2S2[:-2,1:-1,:]+F2S2[:-2,2:,:]+F2S2[1:-1,:-2,:] + F2S2[1:-1,1:-1,:] + F2S2[1:-1,2:,:] + F2S2[2:,:-2,:] + F2S2[2:,1:-1,:] + F2S2[2:,2:,:]

        #Total fitness of all members of community
        Pden = F2[:-2,:-2,:]+F2[:-2,1:-1,:]+F2[:-2,2:,:]+F2[1:-1,:-2,:] + F2[1:-1,1:-1,:] + F2[1:-1,2:,:] + F2[2:,:-2,:] + F2[2:,1:-1,:] + F2[2:,2:,:]

        P = (P/Pden)*g + 0.5*(1.0-g) #probability matrix
        #---------

        #Set new affiliations based on probability matrix and random numbers stored in R
        S[:,:,:] = 0
        S[R<=P] = 1

        fc_ave[t+1] = S.sum()
        #----Finish time marching-----

    fc_ave = fc_ave*N2inv/m

    return S,fc_ave
#------------------

def performance(input=(None),display=True):
    """Assess performance of simulate2, simulate2_f90, and simulate2_omp.

    In this part, we fixed b=1.1, e=0.01, g=0.95, Nt=100.
    Figure 1 (also fixing N=21) and Figure 3 (also fixing m=300) compares wallclock time of different implementations against m and N respectively.
    Clearly, using only python spends much more time than the others, since for loop in python is inefficient.
    Curves representing simulate2_f90 (using fortran only) and simulate2_omp with 1 thread almost coincide,
    implying they have similar performance, because do loop and vectorization in fortran spend similar length of time.
    Simulate2_omp with 2 threads is the most efficient way since we parallelized do loops.
    As N or m get larger, it always cost less time compared with 1 thread or the other method.
    Therefore, we then compute and plot the speed-up against m or N, displaying in Figure 2 and Figure 4 respectively.
    And it is clear that simulate2_omp with 2 threads is approximately 1.3 times faster than 1 thread.
    """
    #declare variables
    b=1.1
    tr.tr_b = 1.1
    e=0.01
    tr.tr_e = 0.01
    g=0.95
    tr.tr_g = 0.95

    #plot wallclock time against m with different coding methods
    performance_figure1(b,e,g)

    #plot wallclock time against N with different coding methods
    performance_figure2(b,e,g)

    return None
#------------------

def performance_figure1(b,e,g):
    """Plot wallclock time against m with different coding methods."""
    N = 21
    Nt = 100
    M = range(0,10001,500)
    time_py = []
    time_f90 = []
    time_omp2 = []
    time_omp1 = []
    for m in M[1:len(M)]:
        print("computing wallclock times against m = %d" %m)

        #time python code
        py_t1 = time.time() #python start time
        simulate2(N,Nt,b,e,g,m)
        py_t2 = time.time() #python t2-t1 gives wallclock time
        time_py.append(py_t2-py_t1)

        #time fortran f90 code
        f90_t1 = time.time() #fortran start time
        tr.simulate2_f90(N,Nt,m)
        f90_t2 = time.time() #fortran t2-t1 gives wallclock time
        time_f90.append(f90_t2-f90_t1)

        #time fortran+openmp code with 2 threads
        tr.numthreads = 2
        omp2_t1 = time.time() #fortran+openmp start time
        tr.simulate2_omp(N,Nt,m)
        omp2_t2 = time.time() #fortran+openmp t2-t1 gives wallclock time
        time_omp2.append(omp2_t2-omp2_t1)

        #time fortran+openmp code with 1 thread
        tr.numthreads = 1
        omp1_t1 = time.time() #fortran+openmp start time
        tr.simulate2_omp(N,Nt,m)
        omp1_t2 = time.time() #fortran+openmp t2-t1 gives wallclock time
        time_omp1.append(omp1_t2-omp1_t1)
    print("plotting Figure 1")
    plt.figure()
    plt.plot(M[1:len(M)],time_py,"-o",M[1:len(M)],time_f90,"-o",M[1:len(M)],time_omp2,"-o",M[1:len(M)],time_omp1,"-o")
    plt.title("Figure 1: Wallclock time against m with different coding methods. Plotting from function performance_figure1. Produced by Yage Hao.")
    plt.xlabel("m, number of trials")
    plt.ylabel("wallclock time")
    plt.legend(labels=["time_py","time_f90","time_omp2","time_omp1"],loc="best")
    plt.show()

    print("plotting Figure 2")
    plt.figure()
    plt.plot(M[1:len(M)],np.array(time_omp1)/np.array(time_omp2),"-o")
    plt.title("Figure 2: Speed-up against m of omp 2 threads compared with omp 1 thread. Plotting from function performance_figure2. Produced by Yage Hao.")
    plt.xlabel("m, number of trials")
    plt.ylabel("speed-up")
    plt.ylim(0,4)
    plt.show()
    return None
#------------------

def performance_figure2(b,e,g):
    """Plot wallclock time against N with different coding methods."""
    Nvec = range(1,102,10)
    Nt = 100
    m = 300
    time_py = []
    time_f90 = []
    time_omp2 = []
    time_omp1 = []
    for N in Nvec[1:len(Nvec)]:
        print("computing wallclock times against N = %d" %N)
        #time python code
        py_t1 = time.time() #python start time
        simulate2(N,Nt,b,e,g,m)
        py_t2 = time.time() #python t2-t1 gives wallclock time
        time_py.append(py_t2-py_t1)

        #time fortran f90 code
        f90_t1 = time.time() #fortran start time
        tr.simulate2_f90(N,Nt,m)
        f90_t2 = time.time() #fortran t2-t1 gives wallclock time
        time_f90.append(f90_t2-f90_t1)

        #time fortran+openmp code with 2 threads
        tr.numthreads = 2
        omp2_t1 = time.time() #fortran+openmp start time
        tr.simulate2_omp(N,Nt,m)
        omp2_t2 = time.time() #fortran+openmp t2-t1 gives wallclock time
        time_omp2.append(omp2_t2-omp2_t1)

        #time fortran+openmp code with 1 thread
        tr.numthreads = 1
        omp1_t1 = time.time() #fortran+openmp start time
        tr.simulate2_omp(N,Nt,m)
        omp1_t2 = time.time() #fortran+openmp t2-t1 gives wallclock time
        time_omp1.append(omp1_t2-omp1_t1)
    print("plotting Figure 3")
    plt.figure()
    plt.plot(Nvec[1:len(Nvec)],time_py,"-o",Nvec[1:len(Nvec)],time_f90,"-o",Nvec[1:len(Nvec)],time_omp2,"-o",Nvec[1:len(Nvec)],time_omp1,"-o")
    plt.title("Figure 3: Wallclock time against N with different coding methods. Plotting from function performance_figure2. Produced by Yage Hao.")
    plt.grid()
    plt.xlabel("N")
    plt.xticks([11,21,31,41,51,61,71,81,91,101])
    plt.ylabel("wallclock time")
    plt.legend(labels=["time_py","time_f90","time_omp2","time_omp1"],loc="best")
    plt.show()

    print("plotting Figure 4")
    plt.figure()
    plt.plot(Nvec[1:len(Nvec)],np.array(time_omp1)/np.array(time_omp2),"-o")
    plt.title("Figure 4: Speed-up against N of omp 2 threads compared with omp 1 thread. Plotting from function performance_figure2. Produced by Yage Hao.")
    plt.grid()
    plt.xlabel("N")
    plt.xticks([11,21,31,41,51,61,71,81,91,101])
    plt.ylabel("speed-up")
    plt.ylim(0,4)
    plt.show()
    return None
#------------------

def analyze(input=(None),display=True):
    """Analyze influence of model parameter, g.

    When plotting Figure 5, we fixed N=51, Nt=1000, m=100, e=0.01 and plotted 6 curves with different b values.
    All these curves are average fc after Nt generations against parameter g.
    Their trends are first decreasing slowly until reaching a minimum (with corresponding g between 0.975 and 1),
    then increasing quickly.
    Also we find that the curve with smaller b value always have larger average fc_final if we fix parameter g.
    Specifically, average fc_final, means the probability that all villages belong to C after Nt generations.
    And case gamma=1 suggests the same result as our Homework 1 model that
    when b increases C villages will be less successful.
    """
    #declare variables
    tr.tr_e = 0.01
    tr.numthreads = 2
    N = 51
    Nt = 1000
    m = 100
    B = np.linspace(1.00,1.50,6)
    B[0] = 1.01
    G = np.linspace(0.8,1.0,21)

    plt.figure()
    for tr.tr_b in B:
        print("simulating with b=%f" %tr.tr_b)
        fc_final = []
        for tr.tr_g in G:
            print("g=%f" %tr.tr_g)
            fc_final.append(tr.simulate2_omp(N,Nt,m)[1][-1])
        plt.plot(G,fc_final,"-o")
    print("plotting Figure 5")
    plt.title("Figure 5: Average fraction of C villages after 1000 generations against parameter gamma with different parameter b. Plotting from function analyze. Produced by Yage Hao.")
    plt.xlabel("g, parameter gamma")
    plt.ylabel("fc_final, average fraction of C villages")
    plt.legend(labels=["b=1.01","b=1.10","b=1.20","b=1.30","b=1.40","b=1.50"],loc="best")
    plt.show()

    return None
#------------------

def visualize():
    """Generate an animation illustrating the evolution of
        villages during C vs M competition
    """
    tr.tr_b = 1.0000001
    tr.tr_e = 0.01
    tr.tr_g = 0.95
    nt = 1000
    S = tr.visualizes(21,nt,1)

    #Set up initial figure
    fig, ax = plt.subplots()
    avi = plot_S(S[:,:,0])

    def updatefig(i):
        print("generation=",i)
        avi = plot_S(S[:,:,i])
        return avi

    ani = animation.FuncAnimation(fig, updatefig, frames=nt, interval=100, repeat=False)
    ani.save('hw3movie1.mp4',writer="ffmpeg")
    plt.show()
    return ani

if __name__ == '__main__':
    #Modify the code here so that it calls performance analyze and
    # generates the figures that you are submitting with your code

    performance(input=(None),display=True)

    analyze(input=(None),display=True)

    visualize()
